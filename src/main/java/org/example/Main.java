package org.example;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.di.Container;
import org.example.framework.di.processor.BeanPostProcessor;
import org.example.framework.security.ProxyProxy;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.annotation.HasRole;
import org.example.framework.security.middleware.anon.AnonAuthMiddleware;
import org.example.framework.security.middleware.jsonbody.JSONBodyAuthNMiddleware;
import org.example.framework.server.annotation.Controller;
import org.example.framework.server.controller.ControllerRegistrar;
import org.example.framework.server.controller.handler.ReturnValueHandler;
import org.example.framework.server.controller.resolver.ArgumentResolver;
import org.example.framework.server.http.Server;


import java.io.IOException;
import java.util.Arrays;

@Slf4j

public class Main {
    public static void main(String[] args) throws IOException {
        final Container container = new Container();
        container.register("org.example");
        container.register(Gson.class);
        container.register(JSONBodyAuthNMiddleware.class);

        //@HasRole
        container.register((BeanPostProcessor) (bean,originalClazz) -> {

            final boolean match = Arrays.stream(originalClazz.getDeclaredMethods())
                    .anyMatch(o -> o.isAnnotationPresent(HasRole.class));
            if (match) {
                return ProxyProxy.securedProxy(bean, originalClazz);
            }
            return bean;
        });

        //@Audit
        container.register((BeanPostProcessor) (bean,originalClazz) -> {
            final boolean match = Arrays.stream(originalClazz.getDeclaredMethods())
                    .anyMatch(o -> o.isAnnotationPresent(Audit.class));
            if (match) {
                return ProxyProxy.loggedProxy(bean, originalClazz);
            }
            return bean;
        });

        container.wire();

        final Server server = Server.builder()

                .middleware((JSONBodyAuthNMiddleware)container.getBean(JSONBodyAuthNMiddleware.class.getName()))
                .middleware(new AnonAuthMiddleware())
                .argumentResolvers(container.getBeansByType(ArgumentResolver.class))
                .returnValueHandlers(container.getBeansByType(ReturnValueHandler.class))
                .router(new ControllerRegistrar().register(container.getBeansByAnnotation(Controller.class)))
                .build();

        server.start(8081);
        try {
            Thread.sleep(1000000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        server.stop();
    }
}

