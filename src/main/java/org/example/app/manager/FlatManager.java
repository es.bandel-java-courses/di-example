package org.example.app.manager;

import org.example.app.dto.FlatCreateOrUpdateRQ;
import org.example.app.dto.FlatGetRS;

import java.util.List;

public interface FlatManager {
    List<FlatGetRS> getAll();

    FlatGetRS create(FlatCreateOrUpdateRQ requestDTO);
}
