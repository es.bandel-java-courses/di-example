package org.example.app.manager;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.domain.User;
import org.example.app.dto.GetUserByIdRS;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.app.exception.EasyPasswordException;
import org.example.app.exception.ForbiddenLoginException;
import org.example.app.exception.ItemNotFoundException;
import org.example.app.exception.LoginAlreadyRegisteredException;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.auth.AuthenticationToken;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.server.exception.UnsupportAuthenticationToken;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Slf4j
@NoArgsConstructor
@Component
public class DefaultUserManager implements UserManager {
    private final AtomicLong nextId = new AtomicLong(1);
    private static final Map<String, User> usersByLogin = new HashMap<>();
    private final Map<String, String> tokens = new HashMap<>();
    private static final Map<Long, User> usersById = new HashMap<>();
    private final PasswordEncoder encoder = new Argon2PasswordEncoder();
    private List<String> forbiddenLogins;
    private List<String> topPasswords;
    private boolean isRead = false;

    @Override
    @Audit
    public UserRegisterRS create(final UserRegisterRQ requestDTO) {
        if (isRead == false) {
            try {
                forbiddenLogins = Files.newBufferedReader(Paths.get("forbiddenLogin.txt"))
                        .lines()
                        .collect(Collectors.toList());

                topPasswords = Files.newBufferedReader(Paths.get("topPassword.txt"))
                        .lines()
                        .collect(Collectors.toList());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                isRead = true;
            }
        }

        final String login = requestDTO.getLogin().trim().toLowerCase();
        final String password = requestDTO.getPassword();

        try {
            final boolean isForbiddenLogin = forbiddenLogins.stream().anyMatch(o -> o.equals(login));
            if (isForbiddenLogin) {
                log.error("this login is forbidden: {}", login);
                throw new ForbiddenLoginException("this login is forbidden");
            }
            final boolean isTopPassword = topPasswords.stream().anyMatch(o -> o.equals(password));
            if (isTopPassword) {
                log.error("your password is too easy: {}", password);
                throw new EasyPasswordException("password is too easy");
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        final String encodedPassword = encoder.encode(password);

        final User user = User.builder()
                .id(nextId.getAndIncrement())
                .login(login)
                .passwordHash(encodedPassword)
                .build();

        synchronized (this) {
            if (usersByLogin.containsKey(login)) {
                log.error("registration with same login twice: {}", login);
                throw new LoginAlreadyRegisteredException();
            }

            usersByLogin.put(user.getLogin(), user);
            usersById.put(user.getId(), user);
        }

        return new UserRegisterRS(user.getId(), user.getLogin());
    }
    @Override
    public GetUserByIdRS getById(final long id) {
        final Optional<User> user;
        synchronized (this) {
            user = Optional.ofNullable(usersById.get(id));
        }

        return user
                .map(o -> new GetUserByIdRS(o.getId(), o.getLogin()))
                .orElseThrow(ItemNotFoundException::new)
                ;
    }

    @Override
    public boolean authenticate(final AuthenticationToken request) {
        if (!(request instanceof LoginPasswordAuthenticationToken)) {
            throw new UnsupportAuthenticationToken();
        }
        final LoginPasswordAuthenticationToken converted = (LoginPasswordAuthenticationToken) request;
        final String login = converted.getLogin();
        final String password = (String) converted.getCredentials();

        final String encodedPassword;
        synchronized (this) {
            if (!usersByLogin.containsKey(login)) {
                return false;
            }
            encodedPassword = usersByLogin.get(login).getPasswordHash();
        }
        return encoder.matches(password, encodedPassword);
    }
}

