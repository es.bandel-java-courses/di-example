package org.example.app.manager;

import org.example.app.domain.Flat;
import org.example.app.dto.FlatCreateOrUpdateRQ;
import org.example.app.dto.FlatGetRS;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.annotation.HasRole;
import org.example.framework.security.auth.SecurityContext;
import org.example.framework.server.exception.UserNotAuthorizedException;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DefaultFlatManager implements FlatManager {
    private final List<Flat> items = new ArrayList<>(100);
    private long nextId = 1;

    @HasRole("ROLE_USER")
    @Audit
    @Override
    public FlatGetRS create(final FlatCreateOrUpdateRQ requestDTO) {
        final Principal principal = SecurityContext.getPrincipal();
        if (principal == null) {
            throw new UserNotAuthorizedException("You must authorize for creating advertisement");
        }
        String ownerName = null;
        ownerName = principal.getName();
        final Flat flat = new Flat(
                nextId++,
                requestDTO.getRoomsAmount(),
                requestDTO.isStudio(),
                requestDTO.isFreePlanned(),
                requestDTO.getPrice(),
                requestDTO.getArea(),
                requestDTO.isHasBalcony(),
                requestDTO.isHasLogia(),
                requestDTO.getFloor(),
                requestDTO.getFloorsInHouse(),
                ownerName
        );
        synchronized (this) {
            items.add(flat);
            return new FlatGetRS(
                    flat.getId(),
                    flat.getRoomsAmount(),
                    flat.isStudio(),
                    flat.isFreePlanned(),
                    flat.getPrice(),
                    flat.getArea(),
                    flat.isHasBalcony(),
                    flat.isHasLogia(),
                    flat.getFloor(),
                    flat.getFloorsInHouse(),
                    flat.isRemoved()
            );
        }
    }

    @Override
    public synchronized List<FlatGetRS> getAll() {
        return items.stream()
                .filter(o -> !o.isRemoved())
                .map(o -> new FlatGetRS(
                        o.getId(),
                        o.getRoomsAmount(),
                        o.isStudio(),
                        o.isFreePlanned(),
                        o.getPrice(),
                        o.getArea(),
                        o.isHasBalcony(),
                        o.isHasLogia(),
                        o.getFloor(),
                        o.getFloorsInHouse(),
                        o.isRemoved()
                ))
                .collect(Collectors.toList());
    }
}
