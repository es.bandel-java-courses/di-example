package org.example.app.exception;

public class EasyPasswordException extends RuntimeException {
    public EasyPasswordException() {
    }

    public EasyPasswordException(String message) {
        super(message);
    }

    public EasyPasswordException(String message, Throwable cause) {
        super(message, cause);
    }

    public EasyPasswordException(Throwable cause) {
        super(cause);
    }

    public EasyPasswordException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
