package org.example.app.exception;

public class ForbiddenLoginException extends RuntimeException {
    public ForbiddenLoginException() {
    }

    public ForbiddenLoginException(String message) {
        super(message);
    }

    public ForbiddenLoginException(String message, Throwable cause) {
        super(message, cause);
    }

    public ForbiddenLoginException(Throwable cause) {
        super(cause);
    }

    public ForbiddenLoginException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
