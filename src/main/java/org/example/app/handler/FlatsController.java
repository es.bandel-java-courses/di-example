package org.example.app.handler;


import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.FlatCreateOrUpdateRQ;
import org.example.app.dto.FlatGetRS;
import org.example.app.manager.FlatManager;
import org.example.framework.di.annotation.Component;
import org.example.framework.server.annotation.Controller;
import org.example.framework.server.annotation.RequestBody;
import org.example.framework.server.annotation.RequestMapping;
import org.example.framework.server.annotation.ResponseBody;
import org.example.framework.server.http.HttpMethods;
import java.io.IOException;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
@Controller

public class FlatsController {
    private final  Gson gson;
    private final  FlatManager manager;

    @RequestMapping(method= HttpMethods.POST, path="^/flats$")
    @ResponseBody
    public FlatGetRS create(@RequestBody final FlatCreateOrUpdateRQ requestDTO) throws IOException {
        return manager.create(requestDTO);
    }

    @RequestMapping(method= HttpMethods.GET, path="^/flats$")
    @ResponseBody
    public List<FlatGetRS> getAll() throws IOException {
        return manager.getAll();
    }
}
