package org.example.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class FlatGetRS {
    private long id;
    private int roomsAmount;
    private boolean isStudio;
    private boolean isFreePlanned;
    private int price;
    private int area;
    private boolean hasBalcony;
    private boolean hasLogia;
    private int floor;
    private int floorsInHouse;
    private boolean removed;
}
